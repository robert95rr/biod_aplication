var calculateProgressive = function (password){
	var pro = 0;

	var regExps = [/[a-z]/,/[A-Z]/,/[0-9]/,/.{8}/,/.{20}/,/[!-//:-@[-`{-ÿ]/];

	regExps.forEach(function(regexp){
		if(regexp.test(password)){
			pro++;
		}
	});
	return{
		value: pro,
		max: regExps.length
	};
};

var checkPasswordStregth = function(password){
	var progress = document.querySelector('#passwordPr'),
		pro = calculateProgressive(this.value);

	progress.value = pro.value;
	progress.max = pro.max;
};

var input = document.querySelector('#haslo');
input.addEventListener('keyup', checkPasswordStregth);
