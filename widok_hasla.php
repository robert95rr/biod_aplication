<?php

	session_start();

	if(!isset($_SESSION['zalogowany']))
	{
		header('Location: index.php');
		exit();
	}

?>
<!DOCTYPE HTML>
<html lang="pl">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
	<title>Bezpieczeństwo i ochrona danych</title>
</head>
<body>


    <nav class="navbar navbar-expand-md navbar-dark bg-dark navbar-fixed-top">
		<div class="container-fluid">
			<div class="navbar-header">
	  <a class="navbar-brand" href="http://localhost/BIOD_aplication/" style="color: #66ffcc">
	    Projekt BIOD#
	  </a>
			</div>
	    <ul class="nav navbar-nav navbar-right">
				<?php
				if(isset($_SESSION['zalogowany']))?>
					<li><a href="http://localhost/BIOD_aplication/widok_hasla.php" style="color:#66ffcc">  Moje hasła |</a></li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<li><a href="http://localhost/BIOD_aplication/logout.php" style="color:#66ffcc"> Wyloguj się</a></li>&nbsp;&nbsp;&nbsp;&nbsp;
					<li><a href="" style="color: #ffff33"><?php echo $_SESSION['user'] ?></a></li>

			</ul>
		</div>
	</nav>

</br></br></br>
<div class="table-responsive container">

<div class="container">
	<h1>Lista haseł</h1>
	<hr>
</br>
<center>

	<?php
		require_once "connect.php";

		try
		{
			$polaczenie = new mysqli($host, $db_user, $db_password, $db_name);
			if($polaczenie->connect_errno!=0)																							//jeśli nie uda się polaczyc z baza, to wyswietli sie ten komunikat!!!!
			{
				throw new Exception(mysqli_connect_errno());																//kod bledu!!!!
			}
			else
			{
				//Wyświetlenie tabeli użytkownicy
				if(isset($_SESSION['zalogowany']))
				{
					$id = $_SESSION['id'];
					//echo $id;exit();
					$rezultat = $polaczenie->query("SELECT * FROM box_passwords WHERE user_id = $id ");
				}
        if(!$rezultat) throw new Exception($polaczenie->error);

				if(mysqli_num_rows($rezultat) > 0)
				{

					echo '<table id="data" class="table table-sm "  width="100%">';
  					echo '<thead class="thead-light">';
    					echo "<tr>";
      					echo "<th>serwis</th>";
								echo "<th>hasło</th>";
								echo "<th> </th>";
    					echo "</tr>";
  					echo "</thead>";
						while($users = $rezultat->fetch_assoc())
						{
						echo "<tbody>";
    					echo "<tr>";
      					echo "<td>".$users['serwis']."</td>";
      					echo "<td>".$users['password']."</td>";
								echo '<td><a type="button" class="btn btn-warning btn-sm" href="http://localhost/BIOD_aplication/edit_pass.php?id=';echo $users['id'];echo'">edytuj</a>&nbsp;
											<a type="button" class="btn btn-danger btn-sm" href="http://localhost/BIOD_aplication/remove_pass.php?id=';echo $users['id'];echo '">usuń</a>
								</td>';
    					echo "</tr>";
					}
					echo "</tbody>";
					echo "</table>";
			}
				$rezultat->free_result();
			}


			$polaczenie->close();
		}
		catch (Exception $e)
    {
      echo '<span style="color:red;">Błąd serwera! Przepraszamy za niedogodności!</span>';
      echo '<br />Informacja developerska: '.$e;
    }

	?>
</br>
</center>
	<a type="button" class="btn btn-primary btn-sm" href="http://localhost/BIOD_aplication/add_pass.php">Dodaj</a><hr>
</div>
</div>



</body>

</html>
