<?php

	session_start();

	if(!isset($_SESSION['udanarejestracja']))
	{
		header('Location: index.php');
		exit();
	}
	else
	{
		unset($_SESSION['udanarejestracja']);
	}

	//Usuwanie zmiennych pamiętających wartości wpisane do formularza
	if(isset($_SESSION['fr_login'])) unset($_SESSION['fr_login']);
	if(isset($_SESSION['fr_email'])) unset($_SESSION['fr_email']);
	if(isset($_SESSION['fr_haslo'])) unset($_SESSION['fr_haslo']);
	if(isset($_SESSION['fr_haslo1'])) unset($_SESSION['fr_haslo1']);
	if(isset($_SESSION['fr_miasto'])) unset($_SESSION['fr_miasto']);
	if(isset($_SESSION['fr_wiek'])) unset($_SESSION['fr_wiek']);
	if(isset($_SESSION['fr_regulamin'])) unset($_SESSION['fr_regulamin']);

	//Usuwanie błędów rejestracji
	if(isset($_SESSION['e_login'])) unset($_SESSION['e_login']);
	if(isset($_SESSION['e_email'])) unset($_SESSION['e_email']);
	if(isset($_SESSION['e_haslo'])) unset($_SESSION['e_haslo']);
	if(isset($_SESSION['e_miasto'])) unset($_SESSION['e_miasto']);
	if(isset($_SESSION['e_wiek'])) unset($_SESSION['e_wiek']);
	if(isset($_SESSION['e_regulamin'])) unset($_SESSION['e_regulamin']);
	if(isset($_SESSION['e_bot'])) unset($_SESSION['e_bot']);

?>
<!DOCTYPE HTML>
<html lang="pl">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
	<title>Bezpieczeństwo i ochrona danych</title>
</head>
<body>

	<nav class="navbar navbar-expand-md navbar-dark bg-dark navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">
  <a class="navbar-brand" href="http://localhost/BIOD_aplication/" style="color: #66ffcc">
    Projekt BIOD#
  </a>
    </div>
    <ul class="nav navbar-nav navbar-right">
      <li><a href="http://localhost/BIOD_aplication/zaloguj_form.php" style="color:#66ffcc"> Zaloguj</a></li>&nbsp;&nbsp;&nbsp;&nbsp;
    </ul>
  </div>
</nav>

</br></br></br></br></br>
<div class="container">

<div class="page-header">


	<h2>Dziękujemy za rejestrację w serwisie!</h2>
  </div><hr></br>
</div>

	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>


</body>

</html>
