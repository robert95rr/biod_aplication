-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 25 Maj 2018, 12:49
-- Wersja serwera: 10.1.31-MariaDB
-- Wersja PHP: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `biod_2018`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `uzytkownicy`
--

CREATE TABLE `uzytkownicy` (
  `id` int(10) NOT NULL,
  `user` varchar(30) NOT NULL,
  `password` varchar(256) NOT NULL,
  `email` varchar(35) NOT NULL,
  `miasto` varchar(25) NOT NULL,
  `wiek` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `uzytkownicy`
--

INSERT INTO `uzytkownicy` (`id`, `user`, `password`, `email`, `miasto`, `wiek`) VALUES
(1, 'robert07', '$2y$10$3p/VZ3dEaXx.TxU8rO3OUO5eDuKd9wR1ffvP/tSgAhYeC6oMUCw6u', 'robert6@wp.pl', 'Kalisz', 19),
(2, 'iza05', '$2y$10$SNIhfy6PPUPe2JrW925HVe/bRBqeVsQ5JR.ISlFVXtqnVLJrkw/5K', 'izab71@gmail.com', 'Warszwa', 26),
(3, 'marty49', '$2y$10$QAuTPwbGraj4.4yeh2Hrv.Noijv.BSqJ413dUcEUo5kMbVY5hOq3i', 'martab8@gmail.com', 'Sopot', 34),
(4, 'blatek91', '$2y$10$teYdM65asPwSQMr77.tTP.EtFhRsMbTPphFV.KwEu1P0eq0sABPPC', 'sensor33@wp.pl', 'Warta', 61),
(5, 'poter006', '$2y$10$v6OcyK3CGLF6rBXby81AY.Dtgdoyc3/X49ch2sH3yC/WhK1fK71bq', 'hporter11@wp.pl', 'Lublin', 42);

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `uzytkownicy`
--
ALTER TABLE `uzytkownicy`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `uzytkownicy`
--
ALTER TABLE `uzytkownicy`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
