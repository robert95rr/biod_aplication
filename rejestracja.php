<?php
 	session_start();

	if(isset($_POST['email']))
	{
		//Udana walidacja!!!
		$wszystko_OK=true;

		//Sprawdzanie poprawności loginu
		$login = $_POST['login'];

		//Sprawdzanie dlugosci loginu
		if((strlen($login)<3) || (strlen($login)>20))
		{
			$wszystko_OK=false;
			$_SESSION['e_login']="Login musi posiadać od 3 do 20 znaków!";
		}
		if(ctype_alnum($login)==false)
		{
			$wszystko_OK=false;
			$_SESSION['e_login']="Login może składać się tylko z liter i cyfr (bez polskich znaków)";
		}

		//Sprawdzanie poprawności adresu email
		$email = $_POST['email'];
		$emailB = filter_var($email, FILTER_SANITIZE_EMAIL);

		if((filter_var($emailB, FILTER_VALIDATE_EMAIL)==false) || ($emailB!=$email))
		{
			$wszystko_OK=false;
			$_SESSION['e_email']="Podaj poprawny adres e-mail!";
		}

		//Srawdzanie poprawności hasła
		$haslo = $_POST['haslo'];
		$haslo1 = $_POST['haslo1'];

		if((strlen($haslo)<8) ||(strlen($haslo)>20))
		{
			$wszystko_OK=false;
			$_SESSION['e_haslo']="Hasło musi posiadać od 8 do 20 znaków!";
		}

		if($haslo!=$haslo1)
		{
			$wszystko_OK=false;
			$_SESSION['e_haslo']="Podane hasła nie są identyczne!";
		}

		$haslo_hash = password_hash($haslo, PASSWORD_DEFAULT);

    //Czy zaakceptowano regulamin?
		if(!isset($_POST['regulamin']))
		{
			$wszystko_OK=false;
			$_SESSION['e_regulamin']="Potwierdź akceptację regulaminu!";
		}

		//Bot or not? Oto jest pytanie
		$sekret_key = "6LdZvloUAAAAAERaRvppD7CP9OjfLJ0BnO2lLuh7";
    $response_key = $_POST['g-recaptcha-response'];
    $user_ip = $_SERVER['REMOTE_ADDR'];

		$sprawdz = "https://www.google.com/recaptcha/api/siteverify?secret=$sekret_key&response=$response_key&remoteip=$user_ip";
    $odpowiedz = file_get_contents($sprawdz);
		$odpowiedz = json_decode($odpowiedz);


		if($odpowiedz->success==false)
		{
			$wszystko_OK=false;
			$_SESSION['e_bot']="Potwierdź, że nie jesteś robotem!";
		}

    //Zapamiętaj wprowadzone dane
    $_SESSION['fr_login'] = $login;
    $_SESSION['fr_email'] = $email;
    $_SESSION['fr_haslo'] = $haslo;
    $_SESSION['fr_haslo1'] = $haslo1;
    if(isset($_POST['regulamin'])) $_SESSION['fr_regulamin'] = true;

    require_once "connect.php";
    mysqli_report(MYSQLI_REPORT_STRICT);

    try
    {
      $polaczenie = new mysqli($host, $db_user, $db_password, $db_name);
      if($polaczenie->connect_errno!=0)																							//jeśli nie uda się polaczyc z baza, to wyswietli sie ten komunikat!!!!
    	{
    		throw new Exception(mysqli_connect_errno());																//kod bledu!!!!
    	}
      else
      {
        //Czy email już istnieje?
        $rezultat = $polaczenie->query("SELECT id FROM users WHERE email='$email'");

        if(!$rezultat) throw new Exception($polaczenie->error);

        $ile_takich_maili = $rezultat->num_rows;
        if($ile_takich_maili>0)
        {
          $wszystko_OK=false;
    			$_SESSION['e_email']="Istnieje już konto przypisane do tego adresu email!";
        }


        //Czy login już istnieje?
        $rezultat = $polaczenie->query("SELECT id FROM users WHERE user='$login'");

        if(!$rezultat) throw new Exception($polaczenie->error);

        $ile_takich_loginow = $rezultat->num_rows;
        if($ile_takich_loginow>0)
        {
          $wszystko_OK=false;
    			$_SESSION['e_login']="Istnieje już użytkownik o takim loginie! Wybierz inny.";
        }

        if($wszystko_OK == true)
        {
          //Wszystko w porządku!!!
          if($polaczenie->query("INSERT INTO users VALUES (NULL, '$login', '$haslo_hash', '$email')"))
          {
            $_SESSION['udanarejestracja']=true;
            header('location: witamy.php');
          }
          else
          {
            throw new Exception($polaczenie->error);
          }
        }


        $polaczenie->close();
      }
    }
    catch (Exception $e)
    {
      echo '<span style="color:red;">Błąd serwera! Przepraszamy za niedogodności i prosimy o rejestrację w innym terminie!</span>';
      echo '<br />Informacja developerska: '.$e;
    }


	}

?>
<!DOCTYPE HTML>
<html lang="pl">
<head>
	<meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
	<title>Bezpieczeństwo i ochrona danych</title>
	<script src='https://www.google.com/recaptcha/api.js'></script>

	<style>
		.error
		{
			color:red;
			margin-top: 10px;
			margin-bottom: 10px;
		}
	</style>
</head>
<body>

  <nav class="navbar navbar-expand-md navbar-dark bg-dark navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">
  <a class="navbar-brand" href="http://localhost/BIOD_aplication/" style="color: #66ffcc">
    Projekt BIOD#
  </a>
    </div>
    <ul class="nav navbar-nav navbar-right">
      <li><a href="http://localhost/BIOD_aplication/zaloguj_form.php" style="color:#66ffcc"> Zaloguj</a></li>&nbsp;&nbsp;&nbsp;&nbsp;
    </ul>
  </div>
</nav>

</br></br></br>
<div class="container">

<div class="page-header">


	<h1>Rejestracja</h1>
  </div><hr></br>


		<form method="post">

      <div class="form-row">
  			<div class="col-sm-12">
			    <label for="login">Login:</label>
          <input type="text" class="form-control form-control-sm" id="login" value="<?php
            if(isset($_SESSION['fr_login']))
            {
              echo $_SESSION['fr_login'];
              unset($_SESSION['fr_login']);
            }
          ?>" name="login" placeholder="Login" autofocus="autofocus" />
        </div>
      </div>

			<?php

				if(isset($_SESSION['e_login']))
				{
					echo '<div class="error">'.$_SESSION['e_login'].'</div>';
					unset($_SESSION['e_login']);
				}

			?>
      <div class="form-row">
        <div class="col-sm-12">
			    <label for="email">E-mail:</label>
          <input type="text" class="form-control form-control-sm" id="email" value="<?php
          if(isset($_SESSION['fr_email']))
          {
            echo $_SESSION['fr_email'];
            unset($_SESSION['fr_email']);
          }
        ?>" name="email" placeholder="E-mail" />
      </div>
    </div>

			<?php

				if(isset($_SESSION['e_email']))
				{
					echo '<div class="error">'.$_SESSION['e_email'].'</div>';
					unset($_SESSION['e_email']);
				}

			?>
      <div class="form-row">
  			<div class="col-sm-12">
			    <label for="haslo">Hasło:</label>
          <input type="password" class="form-control form-control-sm" id="haslo"value="<?php
          if(isset($_SESSION['fr_haslo']))
          {
            echo $_SESSION['fr_haslo'];
            unset($_SESSION['fr_haslo']);
          }
        ?>" name="haslo" placeholder="Hasło" />
      </div>
    </div>
    <div class="form-row">
      <div class="col-sm-12">                                                    <!--Pasek siły hasła!!!!-->
			<progress id="passwordPr" value="0" max="100"></progress>
      </div>
		</div>
			<?php

				if(isset($_SESSION['e_haslo']))
				{
					echo '<div class="error">'.$_SESSION['e_haslo'].'</div>';
					unset($_SESSION['e_haslo']);
				}

			?>
      <div class="form-row">
  			<div class="col-sm-12">
			    <label for="haslo1">Powtórz hasło:</label>
          <input type="password" class="form-control form-control-sm" id="haslo1" value="<?php
          if(isset($_SESSION['fr_haslo1']))
          {
            echo $_SESSION['fr_haslo1'];
            unset($_SESSION['fr_haslo1']);
          }
        ?>" name="haslo1" placeholder="Powtórz hasło" />
      </div>
    </div>

  </br>
      <div class="form-group form-check">
			<label class="form-check-label" for="regulamin">
				<input type="checkbox" class="form-check-input" id="regulamin" name="regulamin" <?php
        if(isset($_SESSION['fr_regulamin']))
        {
          echo "checked";
          unset($_SESSION['fr_regulamin']);
        }
        ?> /> Akceptuję regulamin
			</label>
    </div>
			<?php

				if(isset($_SESSION['e_regulamin']))
				{
					echo '<div class="error">'.$_SESSION['e_regulamin'].'</div>';
					unset($_SESSION['e_regulamin']);
				}

			?>
			<div class="g-recaptcha" data-sitekey="6LdZvloUAAAAAM-YudiE4051Kk1CxRm4y4FZBGB_"></div>
			<?php

				if(isset($_SESSION['e_bot']))
				{
					echo '<div class="error">'.$_SESSION['e_bot'].'</div>';
					unset($_SESSION['e_bot']);
				}

			?>
		</br>
		<button type="submit" class="btn btn-primary btn-sm">Zarejestruj się</button>

  </form></br></br><hr>
</br>

</div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
    <script src="http://localhost/BIOD_aplication/js/power_pass.js"></script>


</body>

</html>
