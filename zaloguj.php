<?php

	session_start();

	if((!isset($_POST['login'])) || (!isset($_POST['haslo'])))										//jeśli nie jest ustawione login i haslo to przekierowujemy do formularza
	{																																							//logowania!!!!
		header('Location: zaloguj_form.php');
		exit();
	}

	require_once "connect.php";
																																								//otwieranie polaczeia!!!!
	$polaczenie = new mysqli($host, $db_user, $db_password, $db_name);						//zablokowanie wyświetlania jakichkolwiek bledow - (@)!!!!

	if($polaczenie->connect_errno!=0)																							//jeśli nie uda się polaczyc z baza, to wyswietli sie ten komunikat!!!!
	{
		echo "Error: ".$polaczenie->connect_errno;																	//kod bledu!!!!
	}
	else
	{
		$login = $_POST['login'];
		$haslo = $_POST['haslo'];
		//alternatywa dla systemu logowania podatnego na wstrzykiwanie SQL!!!!!

		///$sql = "SELECT * FROM uzytkownicy WHERE user='$login' AND password='$haslo'";

		//if($rezultat = @$polaczenie->query($sql));

		//Koniec

		$login = htmlentities($login, ENT_QUOTES, "UTF-8");													//encje html, czyli róznica między kodem żródlowym a tekstem do wyswietlenia!!!
		//$haslo = htmlentities($haslo, ENT_QUOTES, "UTF-8");

		if($rezultat = @$polaczenie->query(sprintf("SELECT * FROM users WHERE user='%s'",
		mysqli_real_escape_string($polaczenie, $login))))															//funkcja zabezpiecza przed wstrzykiwaniem SQL!!!
		//mysqli_real_escape_string($polaczenie, $haslo))))//tu koniec!
		{
			$ilu_userow = $rezultat->num_rows;																				//ilosc zwróconych wierszy (0 albo 1)!!!!
			if($ilu_userow>0)
			{
				$wiersz = $rezultat->fetch_assoc();																			//pobranie wszystkich danych z poszczególnych kolumn w danym wierszu!!!!

				if(password_verify($haslo, $wiersz['password']))
				{

					$_SESSION['zalogowany'] = true;																					//zmienna zapisana w sesji, która mówi nam, że jesteśmy zalogowani!!!!

					$_SESSION['id'] = $wiersz['id'];// do porównania!!!!!!!!!!!!!!!!!!
					$_SESSION['user'] = $wiersz['user'];
					$_SESSION['email'] = $wiersz['email'];

					unset($_SESSION['blad']);																								//usuniecie z sesji zmiennej blad, jeśli jestesmy zalogowani!!!!

					$rezultat->free_result();																								//zwolnienie z pamieci rezultaty zapytania!!!!

					header('Location: widok.php');
				}
				else
				{
					$_SESSION['blad'] = '<span style="color:red">Nieprawidłowy login lub hasło!</span>';
					header('Location: zaloguj_form.php');
				}

			}else
			{
				$_SESSION['blad'] = '<span style="color:red">Nieprawidłowy login lub hasło!</span>';
				header('Location: zaloguj_form.php');
			}
		}

		$polaczenie->close();																												//zamykanie polaczenia!!!!


	}


?>
