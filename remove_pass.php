<?php
 	session_start();

  if(!isset($_SESSION['zalogowany']))
	{
		header('Location: index.php');
		exit();
	}

  require_once "connect.php";
  mysqli_report(MYSQLI_REPORT_STRICT);

  try
  {
    $polaczenie = new mysqli($host, $db_user, $db_password, $db_name);
    if($polaczenie->connect_errno!=0)																							//jeśli nie uda się polaczyc z baza, to wyswietli sie ten komunikat!!!!
    {
      throw new Exception(mysqli_connect_errno());																//kod bledu!!!!
    }
    else
    {
        //Wszystko w porządku!!!
          $id = $_GET['id'];
          if($polaczenie->query("DELETE FROM box_passwords WHERE id = $id "))
          {
            $_SESSION['udaneusunieciehasla']=true;
            header('location: widok_hasla.php');
          }

        else
        {
          throw new Exception($polaczenie->error);
        }
      }

      $polaczenie->close();
    }
  catch (Exception $e)
  {
    echo '<span style="color:red;">Błąd serwera! Przepraszamy za niedogodności i prosimy o dodanie hasła w innym terminie!</span>';
    echo '<br />Informacja developerska: '.$e;
  }



?>

<!DOCTYPE HTML>
<html lang="pl">
<head>
	<meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
	<title>Bezpieczeństwo i ochrona danych</title>
	<script src='https://www.google.com/recaptcha/api.js'></script>

	<style>
		.error
		{
			color:red;
			margin-top: 10px;
			margin-bottom: 10px;
		}
	</style>
</head>
<body>

  <nav class="navbar navbar-expand-md navbar-dark bg-dark navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">
  <a class="navbar-brand" href="http://localhost/BIOD_aplication/" style="color: #66ffcc">
    Projekt BIOD#
  </a>
    </div>
    <ul class="nav navbar-nav navbar-right">
      <?php
      if(isset($_SESSION['zalogowany']))?>
        <li><a href="http://localhost/BIOD_aplication/widok_hasla.php" style="color:#66ffcc">  Moje hasła |</a></li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <li><a href="http://localhost/BIOD_aplication/logout.php" style="color:#66ffcc"> Wyloguj się</a></li>&nbsp;&nbsp;&nbsp;&nbsp;
        <li><a href="" style="color: #ffff33"><?php echo $_SESSION['user'] ?></a></li>
    </ul>
  </div>
</nav>

</br></br></br>
<div class="container">

<div class="page-header">


	<center>
  </div><hr></br>



</br></br><hr>
</br>
</div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
    <script src="http://localhost/BIOD_aplication/js/power_pass.js"></script>


</body>

</html>
